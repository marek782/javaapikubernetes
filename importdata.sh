#!/usr/bin/env bash

mysql -u root -p"$MYSQL_ROOT_PASSWORD" titanic_api < /tmp/schema.sql

mysqlimport -u root -p"$MYSQL_ROOT_PASSWORD" \
    --local \
    --columns=survived,passenger_class,name,sex,age,siblings_or_spouses_aboard,parents_or_children_aboard,fare \
    --fields-terminated-by="," \
    --ignore-lines=1 titanic_api /tmp/people.csv