FROM gradle:jdk11 as builder
VOLUME /tmp
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build -x test


FROM openjdk:11-jre-slim
VOLUME /tmp
COPY --from=builder --chown=www-data:www-data /home/gradle/src/build/libs/api-1.0.jar app.jar
USER www-data
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]