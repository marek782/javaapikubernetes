import com.api.ApiApplication
import com.api.domain.model.Person
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(ApiApplication::class), webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class ApiTests {

    @Autowired
    private val mvc: MockMvc? = null

    @Test
    fun listPeopleTest() {
        this.mvc!!.perform(get("/people")).andExpect(status().isOk())
    }

    @Test
    fun createPersonTest() {

        val person = Person(UUID.randomUUID().toString(), false, 2, "Thomas Kowalsky", "male".toCharArray(), 29, 7.40, 0, 0)

        val b = ObjectMapper().writeValueAsBytes(person)

        this.mvc!!.perform(post("/people")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .characterEncoding("UTF-8")
            .content(b)
        ).andExpect(status().isOk())
    }


}