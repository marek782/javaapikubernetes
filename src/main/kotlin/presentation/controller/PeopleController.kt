package com.api.presentation.controller

import com.api.domain.model.Person
import com.api.domain.repository.PeopleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.function.Supplier

@RestController
@RequestMapping("/people")
class PeopleController {


    @Autowired
    lateinit var peopleRepository: PeopleRepository

    @GetMapping("")
    @ResponseBody
    fun list(): Iterable<Person>
    {
        return peopleRepository.findAll()
    }

    @GetMapping("/{id}")
    @ResponseBody
    fun get(@PathVariable id: String): Person
    {
        return peopleRepository.findById(id).orElseThrow((Supplier {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }))
    }

    @PostMapping("")
    @ResponseBody
    fun post(@RequestBody person: Person): Person
    {
        return peopleRepository.save(person)
    }

    @PutMapping("/{id}")
    fun put(@PathVariable id: String, @RequestBody person: Person)
    {
        var personE = peopleRepository.findById(id)

        if (!personE.isEmpty()) {
            person.uuid = id
            peopleRepository.save(person)
        }

        else
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "resource not found")
    }

    @DeleteMapping("/{id}")
    fun put(@PathVariable id: String)
    {
        peopleRepository.deleteById(id)
    }


}